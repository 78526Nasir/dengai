# DengAI

### A Predictive Model for Dengue Fever Spreading Detection using Multiple Supervised Machine Learning Algorithm
---

**The Novelty of this Research:**

* Total **6** different data preprocessing techniques were used in this research:
    * Label Encoding
    * Pearson's Correlation Coefficient
    * Imputation
    * Min-Max Scalar
    * Tukey method for Outliers
    * Chi-Square Test
* In this research, we investigated that **Reduced Feature** with the **highest variance** outperforms **Optimum Features**. 
* We implemented a Novel model that **outperforms** state-of-the-art models to detect **Dengue Fever Spreading**.
    